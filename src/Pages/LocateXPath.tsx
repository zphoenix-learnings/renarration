import React, { useState } from "react";

const LocateXPath = ({ data }: { data?: any }) => {
  const [url, setUrl] = useState("");
  const [article, setArticle] = useState<any>(null);
  const [error, setError] = useState<any>(null);

  const handleSubmit = async (event?: React.FormEvent<HTMLFormElement>) => {
    const xpath = url;

    const result = document.evaluate(
      xpath,
      document,
      null,
      XPathResult.FIRST_ORDERED_NODE_TYPE,
      null
    );

    const element = result.singleNodeValue;

    if (element instanceof HTMLElement) {
      console.log("Element found:", element);
      setArticle(element);
      setError("");
      element.style.backgroundColor = "yellow";
      const hoverEvent = new MouseEvent("mouseover", {
        bubbles: true,
        cancelable: true,
        view: window,
      });
      element.dispatchEvent(hoverEvent);
    } else {
      console.log("Element not found");
      setError("Element not found");
    }
  };

  return (
    <div className="p-20 space-x-5 max-w-3xl py-8 mx-auto">
      <form
        onSubmit={(e) => {
          handleSubmit(e);
        }}
      >
        <label className="gap-5">
          URL:
          <input
            type="text"
            value={url}
            onChange={(e) => setUrl(e.target.value)}
            className="p-2"
          />
        </label>
        <button type="submit" className="p-2 bg-blue-600 rounded text-white">
          Find XPath
        </button>
      </form>
      {article && (
        <div dangerouslySetInnerHTML={{ __html: article }} className="prose" />
      )}
      {error && <div className="">{error}</div>}
    </div>
  );
};
export default LocateXPath;
