import axios from "axios";
import React, { useState, useEffect } from "react";
import { Select, Space } from "antd";
import { useNavigate } from "react-router-dom";
import Home from "./Home";

const { Option } = Select;

const Render = () => {
  const [data, setData] = useState<any>(null);
  const [comment, setComment] = useState<any>([]);
  const [renJSON, setRenJSON] = useState<any>([]);
  const [error, setError] = useState<any>(null);

  const navigate = useNavigate();
  const baseURL = localStorage.getItem("prod_url");
  const readURL = localStorage.getItem("read_url");

  useEffect(() => {
    const params = new URLSearchParams(window.location.search);

    const jsonData = params.get("data");

    if (jsonData !== null) {
      const data = JSON.parse(jsonData);
      getData(data?.url);
      setComment(data);
    }
  }, []);

  useEffect(() => {
    getRenarration();
  }, [comment]);

  const getRenarration = () => {
    const options = {
      method: "GET",
      url: `${baseURL}/renarration?url=${comment?.url}`,
    };

    axios
      .request(options)
      .then((response) => {
        setRenJSON(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const getData = (url: string) => {
    const options = {
      method: "POST",
      url: `${readURL}/api/parse`,
      headers: { "Content-Type": "application/json" },
      data: {
        url: url,
      },
    };

    axios
      .request(options)
      .then(function (response) {
        localStorage.setItem("readability", JSON.stringify(response.data));
        setData(response.data);
        handleSubmit(comment?.xpath, comment?.rennarated_text);
      })
      .catch(function (error) {
        console.error(error);
      });
  };

  useEffect(() => {
    if (comment !== null && comment !== undefined) {
      handleSubmit(comment?.xpath, comment?.rennarated_text);
    }
  }, [data]);

  const handleSubmit = async (renXPath?: any, renTitle?: any) => {
    const xpath = renXPath;

    const result = document.evaluate(
      xpath,
      document,
      null,
      XPathResult.FIRST_ORDERED_NODE_TYPE,
      null
    );
    const element = result.singleNodeValue;

    if (element instanceof HTMLElement) {
      console.log("Element found:", element);

      element.style.backgroundColor = "yellow";
      element.innerHTML = renTitle;

      // element.style.marginRight = "10%";

      // const spaceElement = document.createElement("span");
      // spaceElement.style.marginLeft = "10%";
      // spaceElement.style.position = "absolute";
      // spaceElement.style.right = "0";
      // element.appendChild(spaceElement);

      // const titleElement = document.createElement("span");

      // titleElement.innerHTML = renTitle;
      // spaceElement.appendChild(titleElement);

      const hoverEvent = new MouseEvent("mouseover", {
        bubbles: true,
        cancelable: true,
        view: window,
      });
      element.dispatchEvent(hoverEvent);
    } else {
      console.log("Element not found");
      setError("Element not found");
    }
  };

  const renderData = () => {
    if (!data) {
      return null;
    }

    const { content, title } = data;

    const handleChange = (value: any) => {
      navigate(`/ren/${value}`);
    };

    return (
      <div className="p-5 space-x-5 max-w-7xl py-8 mx-auto">
        <div className="flex items-center justify-center gap-5">
          <form
            onSubmit={() => {
              handleSubmit();
            }}
            className="flex gap-5 w-full"
          >
            <Select
              style={{ width: "100%" }}
              placeholder="Select Article"
              onSelect={handleChange}
              optionLabelProp="label"
              showSearch
              autoClearSearchValue={false}
            >
              {renJSON.length > 0 &&
                renJSON.map((ren, i) => (
                  <Option value={ren?.uuid} label={ren?.title} key={i}>
                    <Space>{ren?.title}</Space>
                  </Option>
                ))}
            </Select>
          </form>
        </div>
        <Home
          title={title}
          textData={content}
          renUrl={comment?.url}
          updateRenarration={() => {
            // getRenarration();
            // handleSubmit();
          }}
          addRen={false}
          addRenTitle=""
        />

        {error && <div className="">{error}</div>}
      </div>
    );
  };

  return <div>{renderData()}</div>;
};

export default Render;
