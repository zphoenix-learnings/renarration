import axios from "axios";
import React, { useEffect, useState, useRef, Fragment } from "react";
import Home from "./Home";
import { Select, Space, Input, Button, Typography, Skeleton } from "antd";
import { useNavigate, useParams, Link } from "react-router-dom";
import type { InputRef } from "antd";

import { RxCross2 } from "react-icons/rx";
import { Disclosure, Menu, Transition } from "@headlessui/react";
import { MdKeyboardArrowDown } from "react-icons/md";
import { VscThreeBars } from "react-icons/vsc";

const { Text } = Typography;

const Renarrate = () => {
  const [url, setUrl] = useState<string>("");
  const [article, setArticle] = useState<any>(null);
  const [error, setError] = useState(null);
  const [renJSON, setRenJSON] = useState<any>([]);
  const inputRef = useRef<InputRef>(null);
  const baseURL = localStorage.getItem("prod_url");
  const readURL = localStorage.getItem("read_url");
  const readability = localStorage.getItem("readability");
  const [urlParam, setUrlParam] = useState<string>("");
  const [read, setRead] = useState<any>();
  const [loading, setLoading] = useState<boolean>(true);
  const [renTitleModal, setRenTitleModal] = useState(false);
  const [addRenTitle, setAddRenTitle] = useState<string>("");
  const [renTitle, setRenTitle] = useState<string>("");
  const [data, setData] = useState<any>(null);

  const navigate = useNavigate();

  useEffect(() => {
    const params = new URLSearchParams(window.location.search);
    const url = params.get("url");
    if (url) {
      setUrlParam(url);
      setUrl(url);
    }

    if (readability !== null) {
      setRead(JSON.parse(readability));
    }
  }, []);

  useEffect(() => {
    const param = new URLSearchParams(window.location.search);

    const jsonData = param.get("data");

    if (jsonData !== null) {
      const data = JSON.parse(jsonData);
      setUrl(data?.url);
      setData(data);
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    if (urlParam !== "") {
      handleSubmit();
      getRenarration();
    }
  }, [urlParam]);

  const getRenarration = () => {
    const options = {
      method: "GET",
      url: `${baseURL}/renarration?url=${url}`,
    };

    axios
      .request(options)
      .then((response) => {
        setRenJSON(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handleSubmit = async () => {
    setLoading(true);

    const options = {
      method: "POST",
      url: `${readURL}/api/parse`,
      headers: { "Content-Type": "application/json" },
      data: { url: url },
    };

    axios
      .request(options)
      .then(function (response) {
        localStorage.setItem("readability", JSON.stringify(response.data));
        setRead(response.data);
        getRenarration();
        setLoading(false);
        data !== null && navigate(`/renarrate?url=${url}`);
      })
      .catch(function (error) {
        console.error(error);
        setError(error.response.data);
        setLoading(false);
      });
  };

  const handleChange = (value: any) => {
    navigate(`/renarrate/${value}`);
  };

  const handleSearch = (event: string) => {
    setUrl(event);
  };

  const onURLChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUrl(event.target.value);
  };

  const ListOfRenarrations = () => {
    return (
      <Menu as="div" className="flex">
        <Menu.Button className="flex text-sm focus:outline-none focus:ring-none focus:ring-none">
          <div className="items-center flex rounded-xl p-2 focus:outline-none ">
            <div className=" flex p-2 text-center items-center justify-center flex-shrink-0 self-center flex-row relative border hover:bg-[#d6ebfd] rounded-full h-8 ">
              <Text style={{ width: 100 }}>Renarrations</Text>
              <MdKeyboardArrowDown size={25} />
            </div>
          </div>
        </Menu.Button>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <div className="text-md font-semibold absolute right-0 px-2 z-10 mt-[3.6rem] min-w-[16rem] max-w-[100%] origin-top-right rounded-md text-black bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none ">
            <span className="my-5 text-xs text-zinc-500 ">
              View existing renarrations
            </span>
            {renJSON.length > 0 &&
              renJSON.map((ren, i) => (
                <Menu.Item key={i}>
                  <div className="my-2">
                    <div className="flex flex-row w-full my-2 text-sm font-normal">
                      <div className="flex flex-col w-full">
                        <div className="p-2 hover:bg-[#acd3ee] flex justify-between">
                          <Link
                            to={`${window.location.origin}/renarrate/${ren?.uuid}`}
                          >
                            <span> {ren?.title}</span>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </Menu.Item>
              ))}
          </div>
        </Transition>
      </Menu>
    );
  };

  const callAddingRen = () => {
    setRenTitle(addRenTitle);
    setRenTitleModal(true);
  };

  const addRenChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setAddRenTitle(event.target.value);
  };

  return (
    <>
      <Disclosure as="nav" className="bg-white shadow-lg sticky top-0 z-10">
        {({ open }) => (
          <>
            <div className="mx-auto max-w-[1536px] px-2 sm:px-6">
              <div className="relative flex h-16 items-center justify-between">
                <div
                  className={`absolute inset-y-0 right-0 justify-end flex items-center sm:hidden`}
                >
                  {/* Mobile menu button*/}
                  <Disclosure.Button className="inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-700 hover:text-white focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                    <span className="sr-only">Open main menu</span>
                    {open ? (
                      <RxCross2
                        className="block h-6 w-6"
                        color="black"
                        aria-hidden="true"
                      />
                    ) : (
                      <VscThreeBars
                        className="block h-6 w-6"
                        color="black"
                        aria-hidden="true"
                      />
                    )}
                  </Disclosure.Button>
                </div>
                <div
                  className={`absolute left-0 justify-start items-center hidden sm:block`}
                >
                  <div className={`justify-start flex items-center `}>
                    <Space style={{ padding: "0 8px 4px" }}>
                      <Input
                        placeholder="Parse article"
                        ref={inputRef}
                        value={url}
                        onChange={onURLChange}
                      />
                      <Button
                        onClick={() => handleSubmit()}
                        type="primary"
                        loading={loading}
                      >
                        Parse Article
                      </Button>
                    </Space>

                    <ListOfRenarrations />
                  </div>
                </div>
                <div
                  className={`absolute right-0 justify-end items-center hidden sm:block`}
                >
                  <div className={`justify-end flex items-center`}>
                    <div className="flex gap-5 p-2">
                      <Input
                        placeholder="Submit the renarration"
                        value={addRenTitle}
                        onChange={addRenChange}
                      />
                      <Button type="primary" onClick={callAddingRen}>
                        Add a label
                      </Button>
                      <Button
                        type="primary"
                        onClick={() => {
                          window.location.reload();
                        }}
                      >
                        Clear
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Disclosure.Panel className="sm:hidden">
              <Space style={{ padding: "0 8px 4px" }}>
                <Input
                  placeholder="Parse article"
                  ref={inputRef}
                  value={url}
                  onChange={onURLChange}
                />
                <Button
                  onClick={() => handleSubmit()}
                  type="primary"
                  loading={loading}
                >
                  Parse Article
                </Button>
              </Space>

              <ListOfRenarrations />

              <div className="flex gap-5 p-2">
                <Input
                  placeholder="Submit the renarration"
                  value={addRenTitle}
                  onChange={addRenChange}
                />
                <Button type="primary" onClick={callAddingRen}>
                  Add a label
                </Button>
                <Button
                  type="primary"
                  onClick={() => {
                    window.location.reload();
                  }}
                >
                  Clear
                </Button>
              </div>
            </Disclosure.Panel>
          </>
        )}
      </Disclosure>
      {loading ? (
        <div className="container pt-10">
          <Skeleton active />
        </div>
      ) : (
        <>
          <Home
            title={read?.title}
            textData={read?.content}
            renUrl={url}
            updateRenarration={() => {
              getRenarration();
              handleSubmit();
            }}
            addRen={renTitleModal}
            addRenTitle={renTitle}
          />
          {error && <div className="">{error}</div>}
        </>
      )}
    </>
  );
};

export default Renarrate;
