import React from "react";
import { Modal } from "antd";
import { CloseOutlined } from "@ant-design/icons";

const ModalComponent = ({
  open,
  onClose,
  onSuccess,
  title,
  width,
  children,
  loading,
  footer,
}: {
  open: boolean;
  onClose: Function;
  onSuccess: Function;
  title?: string;
  width?: string;
  children: React.ReactNode;
  loading?: boolean;
  footer?: any;
}) => {
  return (
    <Modal
      title={title}
      centered
      open={open}
      keyboard={true}
      onOk={() => onSuccess()}
      onCancel={() => onClose()}
      width={width}
      closable={true}
      confirmLoading={loading}
      closeIcon={<CloseOutlined onClick={() => onClose()} />}
      mask
      wrapClassName="fullscreen-modal"
      footer={footer}
    >
      {children}
    </Modal>
  );
};

ModalComponent.defaultProps = {
  title: "",
  width: 1200,
};

export default ModalComponent;
