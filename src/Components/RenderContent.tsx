import React from "react";
import { Typography } from "antd";

const RenderContent = ({
  title,
  content,
}: {
  title: string;
  content: string;
}) => {
  return (
    <>
      <Typography>
        <div className="p-5 space-x-5 max-w-7xl py-8 mx-auto">
          <div className="flex flex-col items-center justify-center">
            <div className="p-2 mx-5 my-5 border rounded-lg  max-w-3xl shadow hover:shadow-lg hover:border-blue-500 transition duration-150 ease-in-out">
              <h1 className="text-3xl font-bold mb-4 flex justify-center">
                {title}
              </h1>
              <div
                className="prose mb-2"
                dangerouslySetInnerHTML={{ __html: content }}
              />
            </div>
          </div>
        </div>
      </Typography>
    </>
  );
};

export default RenderContent;
